#!/usr/bin/env python3

import boto3
import json
import connexion
from apig_wsgi import make_lambda_handler

client = boto3.client('ecs')

def get_cluster_name():
    response_cluster_name = client.list_clusters()
    for k in response_cluster_name['clusterArns']:
        return k

def get_cluster_tags():
    cluster_name = get_cluster_name()
    response_cluster_tags = client.describe_clusters(clusters=[cluster_name],include=['TAGS'])
    for i in response_cluster_tags['clusters']:
        return json.dumps(i['tags'])

def get_service_name():
    cluster_name = get_cluster_name()
    response_service_name = client.list_services(cluster=cluster_name)
    list_service = []
    for j in response_service_name['serviceArns']:
        list_service.append(j)
    return list_service

def get_task_details():
    service_running_pending = {}
    cluster_name = get_cluster_name()
    service_names = get_service_name()
    for service in service_names:
        response_task = client.describe_services(cluster=cluster_name, services=[ service ] )
        for k in response_task['services']:
            service_running_pending[service] = [k['runningCount'],k['pendingCount']]
    return json.dumps(service_running_pending)


app = connexion.App(__name__)
app.add_api('swagger.yaml')

application = app.app

lambda_handler = make_lambda_handler(app)

#if __name__ == '__main__':
#    # run our standalone gevent server
#    app.run(port=8080, server='gevent')
